#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

int main() {
    struct timeval tv;
    gettimeofday(&tv, NULL);

    printf("%ld\n", tv.tv_sec*1000000 + tv.tv_usec);

    return 0;
}
