#!/usr/bin/python
#TODO include utilization data to better understand where bottle necks occur
#TODO add cloning of podman repo
#TODO use podman API?

from datetime import datetime
import argparse
import os
import subprocess
import time
import select
import sys
import csv
from multiprocessing import Process, Array, Queue
from multiprocessing.sharedctypes import RawArray
from ctypes import c_wchar


## Advanced usage only!!
##Functions for timestamps to pickout ensure these are specific and only occur once
#functions = ["podman main()", ">containers.run()", ">ContainerEngine()", "network setup done", "pid ="]
#Use these functions when dropping cache
functions = ["podman main()", ">containers.run()", ">ContainerEngine()", "network setup done", "pid="]


def main():
    #Set everything up
    run_cmd, output_file = setup()

    ##Run test
    print("Running test...")
    for i in range(RUNS):

        #Rebuild after system reset
        if args.custom:
            build_cmd = "./podman/bin/podman build -t hello_time ./hello"
        else:
            build_cmd = "podman build -t hello_time ./hello"
        process = subprocess.Popen(build_cmd.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        time.sleep(2)

        print("Running run " + str(i+1) + " of " + str(RUNS) + " ...")

        run_ends = Array('d', range(CONTAINERS))
        run_starts = Array('d', range(CONTAINERS))
        run_times = Array('d', range(CONTAINERS))
        if args.timestamps:
            run_outputs = [RawArray(c_wchar, 15000) for _ in range(CONTAINERS)]
        else:
            run_outputs = Array('d', range(CONTAINERS))

        #Parallel
        if args.parallel:
            processes = []
            print("Starting " + str(CONTAINERS) + " containers in parallel...")

            #drop cache to emulate cold start
            os.sync()
            with open('/proc/sys/vm/drop_caches', 'w') as f:
                f.write('3')

            overall_start = cont_start = time.time() * 1000000

            for j in range(CONTAINERS):
                p = Process(target=start_container, args=(run_cmd, j, args.parallel, run_outputs))
                processes.append(p)
                cont_start = time.time() * 1000000
                p.start()
                run_starts[j] = cont_start
            for p in processes:
                p.join()

        #Sequential
        else:
            print("Starting " + str(CONTAINERS) + " containers sequentially...")

            #drop cache to emulate cold start
            os.sync()
            with open('/proc/sys/vm/drop_caches', 'w') as f:
                f.write('3')

            overall_start = time.time() * 1000000
            for j in range(CONTAINERS):
                cont_start = time.time() * 1000000
                start_container(run_cmd, j, args.parallel, run_outputs)
                run_starts[j] = cont_start


        #wait for containers to finish starting
        time.sleep(2)
        time.sleep(CONTAINERS*0.2)

        cleanup()

        overall, run_times, func_starts, func_durs = process_data(overall_start, run_outputs, run_starts, run_ends, run_times)

        write_output(output_file, overall, run_times, func_starts, func_durs)


        print("Run " + str(i+1) + " of " + str(RUNS) + " complete.\n")
        time.sleep(10)
        time.sleep(CONTAINERS*0.2)
        #WARNING uncommenting one of these lines will reset podman on your system during the test, including removing containers, pods, images, networks, build cache, machines, and volumes. Only do so if there is noticable performance degradation run to run.
        #process = subprocess.Popen(['echo yes | podman system reset'], shell=True)
        #process = subprocess.Popen(['podman system reset'], shell=True)
        time.sleep(5)


    print("Test complete.\n")

    results(output_file, run_cmd)


def start_container(run_cmd, j, parallel, run_outputs):
    #print("Starting Container " + str(j+1) + " of " + str(CONTAINERS) + " ...")
    process = subprocess.Popen(run_cmd.split(), stdout=subprocess.PIPE)
    output = process.stdout.readline()
    if args.timestamps:
        run_outputs[j].value = output.decode()
    else:
        run_outputs[j] = float(output.decode())


def setup():
    ## Use args to request build types and set parallelism
    parser = argparse.ArgumentParser(prog = 'run.py', description='Run container scaling test.')
    parser.add_argument('containers', metavar='Containers', type=int, help='number of containers to start')
    parser.add_argument('runs', metavar='Runs', type=int, help='number of runs to test')
    parser.add_argument('-c', '--custom', action='store_true', default=False, help='build and use the podman build in the local directory')
    parser.add_argument('-f', '--flags', action='store_true', default=False, help='use optimization flags')
    parser.add_argument('-t', '--timestamps', action='store_true', default=False, help='build and use the podman build in the local directory with timestamp data')
    parser.add_argument('-p', '--parallel', action='store_true', default=False, help='run the test in parallel')

    global args
    args = parser.parse_args()

    global CONTAINERS
    CONTAINERS = args.containers

    global RUNS
    RUNS = args.runs


    with open('./hello/CONTS', 'w+') as f:
        f.write(str(CONTAINERS))

    #parse for kernel param
    kernel=False
    with open("/proc/cmdline", 'r') as file:
        cmdline = file.read()
        if "rcupdate.rcu_normal_after_boot=0" in cmdline:
            kernel=True

    if not os.path.exists('./data'):
        os.makedirs('./data')

    if not os.path.exists('./tmp'):
        os.makedirs('./tmp')

    podman_version = build()


    ##Print headers for output file
    date = datetime.now().strftime("%Y%m%d_%H%M%S")
    output_file = "podman_startup_"+str(date)+"_"+str(CONTAINERS)+"_"+str(RUNS)+"_"+podman_version+"_flags("+str(args.flags)+")_kernel("+str(kernel)+")_parallel("+str(args.parallel)+")"

    with open('data/' + output_file + '.csv', 'w') as f:
        f.write("Overall, 1st Cont, Other Cont, ")
        for func in functions:
            f.write(func + ", ")
            f.write(func + " dur, ")
        f.write("\n")


    ##Setup run command for running podman container
    run_cmd = "podman run --rm"
    output = "Running "
    if args.custom:
        if not os.path.exists('./podman/bin/podman'):
            print("Custom flag requires custom podman build binary to be built, please use make sure a custom podman binary is located at ./podman/bin/podman, reverting to vanilla build")
            args.custom = False
        else:
            run_cmd = "./podman/bin/"+run_cmd
            output = output + "custom containerized build podman version " + podman_version
    else:
        output = output + "vanilla containerized build podman version " + podman_version

    if args.flags:
        run_cmd = run_cmd+" --network=none --transient-store=true"
        output = output + " with optimization flags"

    if args.timestamps:
        if not args.custom:
            print("Timestamps output requires custom podman build at this time, please use make sure a custom podman build is located at ./podman/bin/podman, reverting to default output")
            args.timestamps = False
        else:
            run_cmd = run_cmd+" --timestamps"
            if args.flags:
                output = output + " and timestamps"
            else:
                output = output + " with timestamps"

    run_cmd = run_cmd+" localhost/hello_time"

    return run_cmd, output_file



def build():
    ##Build representative app
    compile_cmd = "gcc hello/hello.c -o hello/hello"
    process = subprocess.Popen(compile_cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    if error:
        print("Error compiling representative application.")
        exit(1)

    ##Build container
    if args.custom:
        if os.path.exists('./podman/bin/podman'):
            print("Building custom build...")
            build_cmd = "./podman/bin/podman build -t hello_time ./hello"
        else:
            print("Custom flag requires custom podman build binary to be built, please use make sure a custom podman binary is located at ./podman/bin/podman, reverting to vanilla build")
            args.custom = False
            build_cmd = "podman build -t hello_time ./hello"
    else:
        print("Building vanilla build...")
        build_cmd = "podman build -t hello_time ./hello"

    process = subprocess.Popen(build_cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if error:
        #TODO get version using git?
        print("Podman build may have failed.")
        exit(1)
    elif not error and args.custom:
        version_cmd = "./podman/bin/podman --version"
    else:
        version_cmd = "podman --version"

    process = subprocess.Popen(version_cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    podman_version=str(output.split()[2].decode())
    print("Podman version: " + podman_version)

    return podman_version


def process_data(overall_start, run_outputs, run_starts, run_ends, run_times):
    #Process timestamp data if requested
    if args.timestamps:
        func_starts = [[0 for k in range(len(functions))]for i in range(CONTAINERS)]
        func_durs = [[0 for k in range(len(functions))]for i in range(CONTAINERS)]
        for j in range(CONTAINERS):
            f = open('./tmp/cont_'+str(j), 'r')
            output = f.readlines()
            #TODO optimize?
            for line in output:
                if '##' not in line and '\x00' not in line:
                    run_ends[j] = float(line)
                for k in range(len(functions)):
                    if functions[k] in line:
                        func_starts[j][k] = line.split()[1]
                        func_durs[j][k] = line.split()[2][:-1]
    else:
        func_starts = None
        func_durs = None

    run_ends = run_outputs
    ##This gives overall start time for both sequential and parallel
    overall_end = max(run_ends)

    for j in range(CONTAINERS):
        cont_start_time = run_ends[j] - run_starts[j]
        print("Container[" + str(j) + "] start time: " + str(cont_start_time))
        run_times[j] = cont_start_time


    overall = overall_end - overall_start
    print("Overall: " + str(overall))

    return overall, run_times, func_starts, func_durs



def write_output(output_file, overall, run_times, func_starts, func_durs):
    with open('data/' + output_file + '.csv', 'a') as f:
        f.write(str(overall)+", ")
        f.write("\n")
        for j in range(CONTAINERS):
            if j == 0:
                f.write(" ,"+str(run_times[j])+", , ")
            else:
                f.write(" , ,"+str(run_times[j])+", ")
            if args.timestamps:
                for k in range(len(functions)):
                    f.write(str(func_starts[j][k]) + ", ")
                    f.write(str(func_durs[j][k]) + ", ")
            f.write("\n")


def cleanup():
    #Clean up any hanging processes
    podman_stop_cmd = 'podman stop -a'
    process = subprocess.Popen(podman_stop_cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    podman_cleanup_cmd = 'podman rm -f -a'
    process = subprocess.Popen(podman_cleanup_cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()


def results(output_file, run_cmd):
    headers = []
    rows = []
    overall_average = 0
    overall_min = sys.maxsize
    overall_max = 0
    container_average = 0
    container_min = sys.maxsize
    container_max = 0
    overall_times = []
    #TODO STD/Deviation %

    with open('data/' + output_file + '.csv', 'r') as f:
        csvfile = csv.reader(f)
        headers = next(csvfile)

        for row in csvfile:
            rows.append(row)

    for row in rows:

        if row[0] != ' ':
            overall_times.append(float(row[0]))
            overall_average += float(row[0])
            overall_min = min(overall_min, float(row[0]))
            overall_max = max(overall_max, float(row[0]))

        elif row[1] != ' ':
            container_average += float(row[1])
            container_min = min(container_min, float(row[1]))
            container_max = max(container_max, float(row[1]))
        else:
            container_average += float(row[2])
            container_min = min(container_min, float(row[2]))
            container_max = max(container_max, float(row[2]))


    overall_average = overall_average / RUNS
    container_average = container_average / (CONTAINERS * RUNS)



    summary_text = "Results summary for starting " + str(CONTAINERS) + " containers "

    if args.parallel:
        summary_text += "in parallel"
    else:
        summary_text += "sequentially"

    summary_text += " using:\n'" + run_cmd + "'\n"

    print("-----------\n")

    print(summary_text)

    print("Overall Average: " + str(overall_average))
    print("Overall Min: " + str(overall_min))
    print("Overall Max: " + str(overall_max))
    print('\n')

    print("Container Average: " + str(container_average))
    print("Container Min: " + str(container_min))
    print("Container Max: " + str(container_max))
    print('\n')

    print("Overall start times by run: ")
    run = 1
    for overall_time in overall_times:
        print("Run " + str(run) + ": " + str(overall_time))
        run+=1

    print("\n-----------")
    print("\nFull results stored at:\n./data/" + output_file + ".csv\n")


if __name__ == '__main__':
    main()
